package com.computerosity.bukkit.adventure.command;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;
import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.Prize;

public class CommandSet {
	AdventurerPlugin plugin;

	public CommandSet(AdventurerPlugin _plugin) {
		plugin = _plugin;
	}

	public void execute(AdventurerPlayer player, String arg) {
		// Check permissions
		if (!player.isPermitted("set"))
			return;

		String split[] = arg.split(" ");
		String token = split[0];

		// Splurge the remaining bits together
		String data = "";
		if (split.length > 1) {
			for (int i = 1; i < split.length; i++)
				data = data + split[i] + " ";
			data = data.trim();
		}

		if (token.equalsIgnoreCase("secret")) {
			setSecret(player, data);
		} else if (token.equalsIgnoreCase("prize")) {
			setPrize(player, data);
		} else if (token.equalsIgnoreCase("name")) {
			setName(player, data);
		} else if (token.equalsIgnoreCase("owner")) {
			setOwner(player, data);
		} else if (token.equalsIgnoreCase("clue")) {
			setClue(player, data);
		} else if (token.equalsIgnoreCase("start")) {
			setStart(player);
		} else if (token.equalsIgnoreCase("spawn")) {

		} else
			player.sendMessage("Syntax: /adv set [secret|prize|name|owner|clue|start]");
	}

	private void setSecret(AdventurerPlayer p, String arg) {
		// if (!p.isPermitted("set.secret")) return;

		if(!p.getIsEditing())
		{
			p.sendMessage("You are not editing a hunt");
			return;
		}
		
		p.getEditedHunt().setSecret(arg);
		p.sendMessage("Secret set to '" + p.getEditedHunt().getSecret() + "'");
	}

	private void setPrize(AdventurerPlayer p, String arg) {
		// if (!p.isPermitted("set.prize")) return;

		// Are we able to edit?
		Adventure editHunt = p.getEditedHunt();
		if (editHunt == null) {
			p.sendMessage("You are not editing a hunt");
			return;
		}

		Player player = p.getPlayer();

		// First, check we're looking at a sign
		Block block = player.getTargetBlock(null, 3);
		if (!(block.getType() == Material.CHEST)) {
			player.sendMessage("Can only set objects of type CHEST as prize");
			return;
		}

		// Get block location
		Location location = block.getLocation();

		// If this item is already in a hunt - don't allow it to be added
		Adventure hunt = plugin.treasureHunts.findHuntByLocation(location);
		if (hunt != null && !editHunt.equals(hunt)) {
			player.sendMessage("This item is part of another treasure hunt");
			return;
		}

		// Item is already set as prize
		ItemStack[] chestContents = null;
		Prize existingPrize = editHunt.getPrize();
		if (existingPrize != null) {
			if (existingPrize.hasLocation(location)) {
				player.sendMessage("Already set as prize");
				return;
			}

			// Copy existing prize contents if required
			if (!arg.equalsIgnoreCase("empty"))
				chestContents = existingPrize.getChestContents();
		}

		// Setup new prize
		Prize prize = new Prize(editHunt, location, null);
		prize.setChestContents(chestContents);
		editHunt.setPrize(prize);

		// Notify success
		player.sendMessage("Prize chest has been set");
	}

	private void setName(AdventurerPlayer p, String arg) {
		// if (!p.isPermitted("set.name")) return;

		if (arg.length() == 0)
			p.sendMessage("Syntax: /adv set name <new name>");
		else {
			if(!p.getIsEditing())
			{
				p.sendMessage("You are not editing a hunt");
				return;
			}
			p.getEditedHunt().setName(arg);
			p.sendMessage("Name set to '" + arg + "'");
		}
	}

	private void setOwner(AdventurerPlayer p, String arg) {
		// if (!p.isPermitted("set.owner")) return;

		if (arg.length() == 0)
			p.sendMessage("Syntax: /adv set owner <new owner name>");
		else {
			if (!p.getIsEditing()) {
				p.sendMessage("You are not editing a hunt");
				return;
			}
			Player found = plugin.getServer().getPlayerExact(arg);
			if (found != null) {
				p.getEditedHunt().setOwner(found.getUniqueId());
				p.sendMessage("Owner set to '" + arg + "'");
			} else {
				p.sendMessage("Could not find player with that name");
			}

		}
	}

	private void setClue(AdventurerPlayer p, String arg) {
		// if (!p.isPermitted("set.clue")) return;

		// Are we able to edit?
		Adventure editHunt = p.getEditedHunt();
		if (editHunt == null) {
			p.sendMessage("You are not editing a hunt");
			return;
		}

		Player player = p.getPlayer();
		boolean isStart = false;

		// First, check we're looking at a sign
		Block block = player.getTargetBlock(null, 3);
		if (!(block.getType() == Material.SIGN_POST) && !(block.getType() == Material.WALL_SIGN)) {
			player.sendMessage("Can only set objects of type SIGN as clue");
			return;
		}

		// Get block location
		Location location = block.getLocation();

		// If this item is already in a hunt - don't allow it to be added
		Adventure hunt = plugin.treasureHunts.findHuntByLocation(location);
		if (hunt != null && !editHunt.equals(hunt)) {
			player.sendMessage("This item is part of another treasure hunt");
			return;
		}

		// Is this a start point, either by request or there isn't one already
		isStart = (arg.equalsIgnoreCase("start") || !editHunt.hasStart());

		// It's already part of our hunt, so the
		// only valid action is marking as start
		if (hunt != null) {
			// If so, we may be wanting to mark it as start
			if (isStart) {
				Clue itm = editHunt.findClueByLocation(location);
				editHunt.setStartClue(itm);
				player.sendMessage("Marking item as start point");
			} else {
				// Otherwise there's nothing to do
				player.sendMessage("Item is already in treasure hunt");
			}
			return;
		}

		// If we've got this far it doesn't exist in any hunt, including our
		Clue itm = new Clue(editHunt, location);
		editHunt.clues.add(itm);
		if (isStart) {
			editHunt.setStartClue(itm);
			player.sendMessage("Sign is now start point of treasure hunt");
		} else {
			player.sendMessage("Sign is now part of treasure hunt");
		}
	}

	private void setStart(AdventurerPlayer p) {

	}
}
