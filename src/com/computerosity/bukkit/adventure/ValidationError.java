package com.computerosity.bukkit.adventure;

import org.bukkit.Location;

import com.computerosity.bukkit.adventure.huntitems.HuntItem;

public class ValidationError
{
	HuntItem item;
	String description;
	Adventure hunt;
	
	public ValidationError(Adventure _hunt, HuntItem _item, String _description)
	{
		description = _description;
		item = _item;
		hunt = _hunt;
	}
	
	public HuntItem getHuntItem()
	{
		return item;
	}
	
	public Location getLocation()
	{
		if(item==null)
			return null;
		else
			return item.location;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public Adventure getHunt()
	{
		return hunt;
	}
}
