package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

public class CommandEdit
{
	private final AdventurerPlugin plugin;

	public CommandEdit(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public void execute(AdventurerPlayer player, String arg)
	{
		if (!(player.isPermitted("owner.edit") || player.isPermitted("edit"))) return;

		// If no hunt name is specified show details of current edit status
		if(arg.length()==0)
		{
			Adventure hunt = player.getEditedHunt();
			if(hunt==null)
				player.sendMessage("You are not editing a hunt");
			else
				player.sendMessage("You are currently editing '" + hunt.getName() + "'");
				
			return;
		}
		
		// If already editing, don't edit
		if(player.isEditing())
		{
			player.sendMessage("Already editing a hunt.");
			return;
		}
		
		// Find the hunt
		Adventure hunt = plugin.treasureHunts.findHuntByName(arg);
		if(hunt==null) 
		{
			player.sendMessage("No hunt was found with that name");
			return;
		}
		
		// Check for hunt editing permissions
		if(hunt.getOwner() == player.getUniqueId() && !player.isPermitted("treasurehunt.edit"))
		{
			player.sendMessage("You are only permitted to edit your own hunts");
			return;
		}
		
		// Request edit on the hunt
		if(!player.editHunt(hunt))
			player.sendMessage("That hunt is being edited by another player");	
		else
			player.sendMessage("Hunt '" + hunt.getName() +"' selected for editing");
	}
}
