package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

// =============================================================
// Class       : CommandNew
//
// Description : 
//
// Author      : Dave Hart
// =============================================================
public class CommandNew
{
	private final AdventurerPlugin plugin;

	public CommandNew(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public void execute(AdventurerPlayer player, String arg)
	{
		//AdventurerProfileMenu m = new AdventurerProfileMenu(plugin);
		//player.getPlayer().openInventory(m.GetInventory());

		if (!player.isPermitted("new")) return;

		// If already editing, don't edit
		if(player.isEditing())
		{
			player.sendMessage("Can't create a hunt while already editing one.");
			return;
		}
		
		// OK, create a new treasure hunt
		Adventure newHunt = new Adventure(player.getUniqueId());
		plugin.treasureHunts.add(newHunt);
		player.editHunt(newHunt);

		// Message to user
		player.sendMessage("'" + newHunt.getName() + "' has been created and selected");
	}
}
