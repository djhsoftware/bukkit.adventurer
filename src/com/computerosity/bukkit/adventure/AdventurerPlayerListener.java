package com.computerosity.bukkit.adventure;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.block.Block;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.Material;

import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.HuntItem;
import com.computerosity.bukkit.adventure.huntitems.ItemType;
import com.computerosity.bukkit.adventure.huntitems.Prize;

// =============================================================
// Class       : TreasureHuntPlayerListener
//
// Description : Implements player listener
//
// Author      : Junkman
// =============================================================
public class AdventurerPlayerListener implements Listener
{
	private final AdventurerPlugin plugin;
	private AdventurerManager hunters=null;
	
	public AdventurerPlayerListener(AdventurerPlugin instance, AdventurerManager treasureHunters)
	{
		plugin = instance;
		hunters = treasureHunters;
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}

    @EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerLogin(PlayerLoginEvent event)
	{
		hunters.onPlayerLogin(event.getPlayer());
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		hunters.onPlayerQuit(event.getPlayer());
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		// Return if not enabled
		if (!plugin.isEnabled()) return;
		
		// Can't see how this would happen??
		Block block = event.getClickedBlock();
		if (block == null) return;

		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			boolean cancel = false;

			// Check if the block is part of a treasure hunt
			Location location = block.getLocation();
			HuntItem item = plugin.treasureHunts.findItemByLocation(location);
			if(item!=null)
			{
				// Register or locate the player as a treasure hunter
				AdventurerPlayer hunter = plugin.treasureHunters.registerPlayer(event.getPlayer());

				// For CHEST interactions, handle prize interaction for player
				if (block.getType().equals(Material.CHEST))
				{
					
					// Need to handle this better
					if(item.getType()!=ItemType.PRIZE)
					{
						plugin.ReportWarning("DB<=>WORLD Prize sync issue (types don't match)");
						return;
					}
					
					// Handle the interaction
					Prize prize = (Prize)item;
					
					// Inventory workaround
					plugin.treasureHunters.syncChestContents(prize);
					
					// Do prize interaction
					cancel = hunter.onPrizeInteract(prize);
					
					// If not cancelled it means we've found the prize, so save updates
					if(!cancel) plugin.dataHandler.saveAdventurer(hunter);
				}

				// For SIGN interactions, handle sign interaction for player
				else if (block.getType().equals(Material.WALL_SIGN) || block.getType().equals(Material.SIGN_POST))
				{
					// Need to handle this better
					if(item.getType()!=ItemType.CLUE)
					{
						plugin.ReportWarning("DB<=>WORLD Clue sync issue (types don't match)");
						return;
					}

					// Handle the clue interaction
					boolean statusChanged = hunter.onClueInteract((Clue)item);
					
					// If the player found a sign, save updates (?? performance issue ??)
					if(statusChanged) plugin.dataHandler.saveAdventurer(hunter);
					
					// Always cancel sign events
					cancel = true;
				}
			}	

			if (cancel) event.setCancelled(true);
		}
	}
}
