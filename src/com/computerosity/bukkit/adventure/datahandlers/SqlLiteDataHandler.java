package com.computerosity.bukkit.adventure.datahandlers;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventureManager;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;
import com.computerosity.bukkit.adventure.AdventurerPlayerQuest;
import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.HuntItem;
import com.computerosity.bukkit.adventure.huntitems.Prize;

//=============================================================
// Class       : Storage
//
// Description : storage handler
//
// Author      : Dave Hart
//=============================================================
public class SqlLiteDataHandler extends DataHandler
{
	private String filename;
	private File sqlFile;

	public SqlLiteDataHandler(AdventurerPlugin thplugin, String dbName)
	{
		type = HandlerType.SQLLITE;
		plugin = thplugin;
		filename = dbName;
		initConnection();
		initDatabase();
	}

	@Override
	public void loadHunts(AdventureManager hunts)
	{
		try
		{
			int count = 0;
			ResultSet result = executeSelect("select * from jadhunt");
			while (result.next())
			{
				// Setup hunt
				Adventure hunt = new Adventure(result.getInt("h_huntid"));
				loadHuntFromSqlResult(result,hunt);

				hunts.add(hunt);
				count++;
			}

			// Load associated info
			result = executeSelect("select * from jadhuntitem");
			while (result.next())
			{
				Adventure hunt = hunts.findHuntByID(result.getInt("hi_huntid"));
				if (hunt != null) loadHuntCluesFromSqlResult(result,hunt);
			}

			// Load chest contents
			for (Adventure hunt : hunts)
			{
				loadChestContents(hunt);
			}
			
			// Validate all hunts
			for (Adventure hunt : hunts)
			{
				boolean validated = hunt.validate();
				
				if(!validated && plugin.autorepair) 
				{
					plugin.WriteToConsole("'" + hunt.getName() + "' failed validation - attempting autorepair");
					validated = hunt.repair();
				}
				
				if(!validated)
				{
					plugin.WriteToConsole("'" + hunt.getName() + "' failed validation - marking as invalid");
				}
			}

			plugin.WriteToConsole("Loaded " + count + " Treasure hunt(s)");
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Load treasure hunts");
		}
		finally
		{
			closeConnection();
		}
	}

	@Override
	public boolean saveHunt(Adventure hunt)
	{
		HuntItem start = hunt.getStart();
		Prize prize = hunt.getPrize();
		if(prize==null || start==null) return false;
		
		String query = "";

		// Clear hunt tables
		deleteHunt(hunt);

		query = "insert into jadhunt (h_huntid, h_name, h_owneruuid, h_secret, h_lastplayeruuid, h_sWorld, h_sx, h_sy, h_sz, h_pworld, h_px, h_py, h_pz) values ";
		query += "(?,?,?,?,?, ?,?,?,?, ?,?,?,?)";

		executeInsert(query, hunt.getID(), hunt.getName(), hunt.getOwner().toString(), hunt.getSecret(), prize.getLastPlayer()!=null ? prize.getLastPlayer().toString() : null, 
				start.world.getName(), start.location.getX(), start.location.getY(), start.location.getZ(), 
				prize.world.getName(), prize.location.getX(), prize.location.getY(), prize.location.getZ());

		// Save items
		for (Clue clue : hunt.clues)
		{
			saveClue(hunt, clue);
		}

		// Save chest contents
		saveChestContents(hunt);
		
		return true;
	}

	private void saveChestContents(Adventure hunt)
	{
		ItemStack[] chestContents = hunt.getPrize().getChestContents();
        if(chestContents!=null)
        {
            for(int i =0;i<chestContents.length;i++)
            {
                if(chestContents[i]!=null)
                {
                    // Insert record
                    String query = "insert into jadhuntchest (hc_huntid,hc_typeid,hc_amount) values (?,?,?)";
                    executeUpdate(query,hunt.getID(),chestContents[i].getTypeId(),chestContents[i].getAmount());
                }
            }
        }
	}

	private void saveClue(Adventure hunt, Clue clue)
	{
	    String query = "insert into jadhuntitem (hi_huntid,hi_itemid,hi_world, hi_x, hi_y, hi_z) values (?,?,?,?,?,?)";
	    executeUpdate(query,clue.getParent().getID(),clue.getId(),clue.location.getWorld().getName(),clue.location.getX(),clue.location.getY(),clue.location.getZ());
	}

	private void loadChestContents(Adventure hunt)
	{
		Prize prize = hunt.getPrize();
		if (prize == null) return;

		Chest chest = prize.getChest();
		if(chest==null) return;
		
		try
		{
			String query = "select count(*) c from jadhuntchest where hc_huntid = ?";
			int count = this.executeScalarInt(query, 0, hunt.getID());
			query = "select * from jadhuntchest where hc_huntid = ?";
			ResultSet result = executeSelect(query, hunt.getID());
			ItemStack[] chestContents = new ItemStack[count];
			int idx=0;
			while (result.next())
			{
				if (result != null)
				{
					chestContents[idx++] = new ItemStack(result.getInt("hc_typeid"), result.getInt("hc_amount"));
				}
			}
			prize.setChestContents(chestContents);
		}
		catch (SQLException ex)
		{
			plugin.reportError(ex, "Error loading chest contents");
		}
	}

	@Override
	public boolean getIsPlayerRegistered(UUID uuid)
	{
		try
		{
			// Check if player is already registered
			String query = "select count(*) c from jadplayer where p_playeruuid = ?";
			int count = executeScalarInt(query, 0, uuid.toString());
			return (count > 0);
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error getting playerIsRegistered");
			return false;
		}
	}

	@Override
	public void loadHunt(Adventure hunt)
	{
		try
		{
			// Load hunt details
			ResultSet result = executeSelect("select * from jadhunt where h_huntid = ?",hunt.getID());
			loadHuntFromSqlResult(result,hunt);

			// Load hunt clues
			hunt.clues.clear();
			result = executeSelect("select * from jadhuntitem where hi_huntid = ?",hunt.getID());
			while (result.next())
			{
				loadHuntCluesFromSqlResult(result,hunt);
			}

			// Load chest contents
			loadChestContents(hunt);
			
			// Validate
			hunt.validate();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error loading hunt");
		}
		finally
		{
			closeConnection();
		}
	}
	
	private void loadHuntFromSqlResult(ResultSet result,Adventure hunt) throws SQLException
	{
		hunt.setName(result.getString("h_name"));
		hunt.setOwner(UUID.fromString(result.getString("h_owneruuid")));
		hunt.setSecret(result.getString("h_secret"));
	
		// Setup prize, invalidate if location isn't a chest
		String pWorld = result.getString("h_pworld");
		double px = result.getDouble("h_px");
		double py = result.getDouble("h_py");
		double pz = result.getDouble("h_pz");
		
		World world = plugin.getServer().getWorld(pWorld);
		Location location = new Location(world, px, py, pz);
		String lp = result.getString("h_lastplayeruuid");
		UUID lastPlayer = lp==null ? null : UUID.fromString(result.getString("h_lastplayeruuid"));
		hunt.setPrize(new Prize(hunt, location, lastPlayer));

		// Setup start position
		String sWorld = result.getString("h_sWorld");
		double sx = result.getDouble("h_sx");
		double sy = result.getDouble("h_sy");
		double sz = result.getDouble("h_sz");

		if (!result.wasNull())
		{
			world = plugin.getServer().getWorld(sWorld);
			location = new Location(world, sx, sy, sz);
			hunt.setStartClue(new Clue(hunt, location));
		}
	}

	private void loadHuntCluesFromSqlResult(ResultSet result,Adventure hunt) throws SQLException
	{
		String iWorld = result.getString("hi_world");
		int itemid = result.getInt("hi_itemid");
		double ix = result.getDouble("hi_x");
		double iy = result.getDouble("hi_y");
		double iz = result.getDouble("hi_z");
		
		World world = plugin.getServer().getWorld(iWorld);
		Location location = new Location(world, ix, iy, iz);
		Clue item = new Clue(hunt, location, itemid, false);
		hunt.clues.add(item);
	}
	

	@Override
	public void deleteHunt(Adventure hunt)
	{
		// Clear hunt tables
		executeDelete("delete from jadhunt where h_huntid = " + hunt.getID());
		executeDelete("delete from jadhuntitem where hi_huntid = " + hunt.getID());
		executeDelete("delete from jadhuntchest where hc_huntid = " + hunt.getID());
	}

	@Override
	public void saveAdventurer(AdventurerPlayer treasureHunter)
	{
		// Get string representation of UUID
		String uuidStr = treasureHunter.getUniqueId().toString();
		
		// Delete existing data
		executeDelete("delete from jadplayer where p_playeruuid = ?",uuidStr);

		// Insert player active hunt
		AdventurerPlayerQuest hunt = treasureHunter.getActiveHunt();
		int huntid = (hunt!=null) ? hunt.getHunt().getID() : 0;
		executeInsert("insert into jadplayer (p_playeruuid,p_activehuntid) values (?,?)", uuidStr, huntid);

		// Delete stats
		executeDelete("delete from jadplayerstats where ps_playeruuid = ?",uuidStr);
		executeDelete("delete from jadplayerchest where pc_playeruuid = ?",uuidStr);
		executeDelete("delete from jadplayerhunts where ph_playeruuid = ?",uuidStr);

		// Iterate through player hunts
		for(AdventurerPlayerQuest tph : treasureHunter.hunts)
		{
			// Write hunt info
			executeInsert("insert into jadplayerhunts (ph_playeruuid,ph_huntid, ph_found, ph_completed) values (?,?,?,?)", 
					uuidStr, tph.getHunt().getID(),
							(tph.getCluesFound() > 0) ? 1 : 0,
							tph.getIsComplete() ? 1 : 0);
			
			// Write clue status to database
			Enumeration<Clue> keys = tph.clues.keys();
			while( keys.hasMoreElements() )
			{
				Clue key = keys.nextElement();
				boolean value = tph.clues.get(key);
				executeInsert("insert into jadplayerstats (ps_playeruuid,ps_huntid, ps_itemid, ps_status) values (?,?,?,?)", 
						uuidStr, huntid, key.getId(),value ? 1 : 0);
			}
			
			// Write chest contents to database
			for(ItemStack stack : tph.playerInventory)
			{
				executeInsert("insert into jadplayerchest (pc_playeruuid,pc_huntid, pc_typeid, pc_amount) values (?,?,?,?)", 
						uuidStr, huntid, stack.getTypeId(),stack.getAmount());
			}
		}
	}

	@Override
	public void saveChestContents(UUID uuid, int huntid, ItemStack[] stacks)
	{
		assert(huntid!=0) : "saveChestContents:Hunt ID is zero";
		
		// Delete old data
		executeDelete("delete from jadplayerchest where pc_huntid = ? and pc_playeruuid = ?",huntid, uuid.toString());

		// Write chest contents to database
		for(ItemStack stack : stacks)
		{
			executeInsert("insert into jadplayerchest (pc_playeruuid,pc_huntid, pc_typeid, pc_amount) values (?,?,?,?)", 
					uuid.toString(), huntid, stack.getTypeId(),stack.getAmount());
		}	
	}
	
	@Override
	public AdventurerPlayer loadAdventurer(Player player)
	{
		// Abandon if null
		if(player==null) return null;
		AdventurerPlayer treasureHunter = null;
	
		try
		{
			// Check if player is registered
			if(getIsPlayerRegistered(player.getUniqueId()))
			{
				// Load player hunts
				treasureHunter = new AdventurerPlayer(player);
				ResultSet result = executeSelect("select * from jadplayerhunts where ph_playeruuid = ?",player.getUniqueId().toString());
				while(result.next())
				{
					int huntid = result.getInt("ph_huntid");
					boolean isComplete = (result.getInt("ph_completed") == 1);
					Adventure hunt = plugin.treasureHunts.findHuntByID(huntid);
					if(hunt!=null)
					{
						AdventurerPlayerQuest tph = new AdventurerPlayerQuest(treasureHunter,hunt);
						tph.setIsComplete(isComplete);
						treasureHunter.hunts.add(tph);
					}
					else
						plugin.WriteToConsole("loadTreasureHunter -> Hunt " + huntid + " not found");
				}
				result.close();
				result=null;
				
				// Load hunt details
				for(AdventurerPlayerQuest tph : treasureHunter.hunts)
				{					
					// Get Hunt
					Adventure hunt = tph.getHunt();
					
					// Load player stats for this hunt
					tph.clues.clear();
					result = executeSelect("select ps_itemid, ps_status from jadplayerstats where ps_playeruuid = ? and ps_huntid = ?",player.getUniqueId().toString(),hunt.getID());
					while(result.next())
					{
						int itemid = result.getInt("ps_itemid");
						boolean status = result.getInt("ps_status")==1;
						
						Clue c = hunt.findClueById(itemid);
						tph.clues.put(c, status);
					}
					result.close();
					result=null;
				
					// Load player chests for this hunt
					tph.playerInventory.clear();
					result = executeSelect("select * from jadplayerchest where pc_playeruuid = ? and pc_huntid = ?",player.getUniqueId().toString(),hunt.getID());
					while(result.next())
					{
						int typeid = result.getInt("pc_typeid");
						int amount = result.getInt("pc_amount");
						ItemStack stack = new ItemStack(typeid,amount);
						tph.playerInventory.add(stack);
					}
					result.close();
					result=null;				
				}
				
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error loading treasure hunter:" + player.getUniqueId());
		}
		finally
		{
			closeConnection();
		}
		
		return treasureHunter;
	}
	
	public void resetDatabase()
	{
		executeDirect("drop table if exists jadhunt");
		executeDirect("drop table if exists jadhuntitem");
		executeDirect("drop table if exists jadhuntchest");
		executeDirect("drop table if exists jadplayer");
		executeDirect("drop table if exists jadplayerstats");
		executeDirect("drop table if exists jadplayerchest");
		executeDirect("drop table if exists jadplayerhunts");
		initDatabase();
	}

	public String getDatabaseVersion()
	{
		try
		{
		return executeScalarString("select dbversion from jadsettings","");
		}
		catch (Exception e)
		{
			plugin.reportError(e, "getDatabaseVersion");
			closeConnection();
			return null;
		}
	}
	
	public void initDatabase()
	{
		String query;

		// Create hunt table
		if (!TableExists("jadhunt"))
		{
			query = "CREATE TABLE jadhunt (h_huntid int NOT NULL, h_name varchar(45) NOT NULL, h_owneruuid varchar(20) NOT NULL, h_secret varchar(6) NOT NULL, h_lastplayeruuid VARCHAR(45) NULL,";
			query += " h_sWorld varchar(20), h_sx double NULL, h_sy double NULL, h_sz double NULL, ";
			query += " h_pWorld varchar(20) NOT NULL, h_px double NOT NULL, h_py double NOT NULL, h_pz double NOT NULL, PRIMARY KEY (h_huntid))";
			executeDirect(query);
		}

		// Create huntitem table
		if (!TableExists("jadhuntitem"))
		{
			query = "CREATE TABLE jadhuntitem (hi_huntid INT NOT NULL, hi_itemid INT NOT NULL, hi_world varchar(20) NULL, hi_x double NULL, hi_y double NULL, hi_z double NULL, PRIMARY KEY (hi_huntid,hi_itemid))";
			executeDirect(query);
		}

		// Create huntchest table
		if (!TableExists("jadhuntchest"))
		{
			query = "CREATE TABLE jadhuntchest (hc_huntid INT NOT NULL, hc_typeid INT NULL, hc_amount INT NULL)";
			executeDirect(query);
		}

		// Create player table
		if (!TableExists("jadplayer"))
		{
			query = "CREATE TABLE jadplayer (p_playeruuid varchar(20),p_activehuntid int, primary key (p_playeruuid))";
			executeDirect(query);
		}

		// Create playerstats table
		if (!TableExists("jadplayerstats"))
		{
			query = "CREATE TABLE jadplayerstats (ps_playeruuid varchar(20),ps_huntid int, ps_itemid int,ps_status int, primary key (ps_playeruuid,ps_huntid,ps_itemid))";
			executeDirect(query);
		}

		// Create playerchests table
		if (!TableExists("jadplayerchest"))
		{
			query = "CREATE TABLE jadplayerchest (pc_playeruuid varchar(20), pc_huntid int, pc_typeid int, pc_amount int)";
			executeDirect(query);
		}

		if (!TableExists("jadplayerhunts"))
		{
			query = "CREATE TABLE jadplayerhunts (ph_playeruuid varchar(20), ph_huntid int, ph_found tinyint, ph_completed tinyint, PRIMARY KEY(ph_playeruuid,ph_huntid))";
			executeDirect(query);
		}

		if (!TableExists("jadsettings"))
		{
			query = "CREATE TABLE jadsettings (dbversion varchar(20))";
			executeDirect(query);

			PluginDescriptionFile pdfFile = plugin.getDescription();
			String pluginVersion = pdfFile.getVersion();
			executeInsert("insert into jadsettings (dbversion) values (?)",pluginVersion);
		}
		
		String ver = getDatabaseVersion();
		plugin.WriteToConsole("Database Schema V" + ver);
		
		// Close connection
		closeConnection();
	}

	private void initConnection()
	{
		try
		{
			sqlFile = new File(plugin.getDataFolder(), filename);
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + sqlFile.getAbsolutePath());
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "SQLiteInit: exception");
		}
		catch (ClassNotFoundException e)
		{
			plugin.reportError(e, "SQLiteInit: class not found");
		}
	}

	private Connection getConnection()
	{
		if (connection == null) initConnection();
		return connection;
	}

	private void closeConnection()
	{
		if (connection == null) return;

		try
		{
			connection.close();
			connection = null;
			sqlFile = null;
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error closing connection");
		}
	}

	private boolean TableExists(String table)
	{
		try
		{
			Connection connection = getConnection();
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet tables;
			tables = dbm.getTables(null, null, table, null);
			return (tables.next());
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error verifying database table: " + table);
		}
		return false;
	}

	private PreparedStatement createPreparedStatement(String query, Object... args)
	{
		try
		{
			int idx = 1;
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(query);
			for (Object obj : args)
			{
				if (obj instanceof String)
					statement.setString(idx++, (String) obj);
				else if (obj instanceof Double)
					statement.setDouble(idx++, (Double) obj);
				else if (obj instanceof Integer)
					statement.setInt(idx++, (Integer) obj);
				else if (obj == null)
					statement.setNull(idx++, java.sql.Types.INTEGER);
				else
					throw new Exception("Haven't catered for this data type in createPreparedStatement!");
			}

			return statement;
		}
		catch (Exception e)
		{
			plugin.reportError(e, "executeNonQuery: " + query);
			closeConnection();
			return null;
		}
	}

	private void executeInsert(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.executeUpdate();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeInsert: " + query);
			closeConnection();
		}
	}

	private void executeDelete(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.execute();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeDelete: " + query);
			closeConnection();
		}
	}

	private void executeUpdate(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.executeUpdate();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeUpdate: " + query);
			closeConnection();
		}
	}

	private void executeDirect(String query)
	{
		try
		{
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			closeConnection();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeDirect: " + query);
			closeConnection();
		}
	}

	private ResultSet executeSelect(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
			{
				Connection connection = getConnection();
				Statement statement = connection.createStatement();
				return statement.executeQuery(query);
			}
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				return statement.executeQuery();
			}
		}
		catch (Exception e)
		{
			plugin.reportError(e, "executeSelect: " + query);
			closeConnection();
			return null;
		}
	}

	private Integer executeScalarInt(String query, int defaultValue, Object... args) throws SQLException
	{
		Integer scalarResult = defaultValue;
		ResultSet result = null;
		try
		{
			result = executeSelect(query, args);
			if (result != null)
			{
				if (result.next()) scalarResult = result.getInt(1);
				result.close();
			}
			closeConnection();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeScalarInt: " + query, false);
		}
		finally
		{
			if (result != null) result.close();
			closeConnection();
		}

		return scalarResult;
	}

	private String executeScalarString(String query, String defaultValue, Object... args) throws SQLException
	{
		String scalarResult = defaultValue;
		ResultSet result = null;
		try
		{
			result = executeSelect(query, args);
			if (result != null)
			{
				if (result.next()) scalarResult = result.getString(1);
				result.close();
			}
			closeConnection();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeScalarInt: " + query, false);
		}
		finally
		{
			if (result != null) result.close();
			closeConnection();
		}

		return scalarResult;
	}
	
	@Override
	public void checkDatabaseVersion() {
		
	}

	@Override
	public void upgradeDatabaseVersion() {
		
	}
}
