package com.computerosity.bukkit.adventure.huntitems;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.computerosity.bukkit.adventure.Adventure;

public abstract class HuntItem
{
	public World world=null;
	public Location location=null;
	protected Integer itemid=0;
	protected Server server = null;
	protected Adventure parent = null;
	protected ItemType type;
	
	abstract public boolean hasLocation(Location location);

	public Adventure getParent()
	{
		return parent;
	}

	public ItemType getType()
	{
		return type;
	}
	
	public int getId()
	{
		return itemid;
	}
	
	public Block getBlock()
	{
		return location.getBlock();
	}
}
