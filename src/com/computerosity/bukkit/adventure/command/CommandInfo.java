package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /adv info commands
//
// Author      : Dave Hart
// =============================================================
public class CommandInfo {
	private final AdventurerPlugin plugin;

	public CommandInfo(AdventurerPlugin plugin) {
		this.plugin = plugin;
	}

	public boolean execute(AdventurerPlayer player, String arg) {
		// Check permissions
		if (!player.isPermitted("info"))
			return true;

		if (!player.isNewbie()) {
			int count = player.getCompletedHuntCount();
			switch (count) {
			case 0:
				player.sendMessage("You have not completed any treasure hunts.");
				break;
			case 1:
				player.sendMessage("You have completed one treasure hunt.");
				break;
			default:
				player.sendMessage("You have completed " + count + " treasure hunt.");
				break;
			}

			count = player.getIncompleteHuntCount();
			switch (count) {
			case 0:
				player.sendMessage("There are no more treasure hunts yet to complete");
				break;
			case 1:
				player.sendMessage("There is one more treasure hunt yet to complete");
				break;
			default:
				player.sendMessage("There are " + count + " more treasure hunts yet to complete");
				break;
			}

			if (player.getActiveHunt() == null)
				player.sendMessage("You are not currently on a treasure hunt");
			else {
				player.sendMessage(
						"You are currently hunting for '" + player.getActiveHunt().getHunt().getName() + "'");
				player.sendMessage("You have found " + player.getActiveHunt().getCluesFound() + " out of "
						+ player.getActiveHunt().getHunt().getClueCount() + " clues");
			}
		} else {
			player.sendMessage("You have not started your treasure hunting career yet!");
			int count = plugin.treasureHunts.size();
			switch (count) {
			case 0:
				player.sendMessage("There are no treasure hunts available");
				break;
			case 1:
				player.sendMessage("There is one treasure hunt available");
				break;
			default:
				player.sendMessage("There are " + count + " treasure hunts available!");
				break;
			}
		}

		return true;
	}
}
