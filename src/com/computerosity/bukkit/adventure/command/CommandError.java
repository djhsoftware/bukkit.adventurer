package com.computerosity.bukkit.adventure.command;

import org.bukkit.Location;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;
import com.computerosity.bukkit.adventure.ValidationError;

public class CommandError
{
	public CommandError(AdventurerPlugin plugin)
	{
		
	}

	public void execute(AdventurerPlayer player,String arg)
	{
		// Check permissions
		if (!player.isPermitted("warp")) return;

		// This is only useable when editing
		Adventure hunt = player.getEditedHunt();
		if(hunt==null)
		{
			player.sendMessage("You need to be editing a validated hunt to use this command");
			return;
		}

		if (arg.equalsIgnoreCase("warp"))
		{
			warpToError(player,hunt);
		}
		else if (arg.equalsIgnoreCase("fix"))
		{
			fixError(player,hunt);
		}
		else if (arg.equalsIgnoreCase("delete"))
		{
			deleteError(player,hunt);
		}
		else if (arg.equalsIgnoreCase("next"))
		{
			nextError(player,hunt);
		}
		else if (arg.equalsIgnoreCase("prev"))
		{
			prevError(player,hunt);
		}
		else if(arg.equalsIgnoreCase("fixall"))
		{
			fixAllErrors(player,hunt);
		}
		else
			player.sendMessage("Syntax: /th error [next|prev|warp|fix|fixall|delete]");
	}

	private void fixAllErrors(AdventurerPlayer player, Adventure hunt)
	{
		hunt.repair();
		
		if(hunt.validate())
			player.sendMessage("Repair was successful");
		else
		{
			hunt.showValidationErrors(player);
			player.sendMessage("Hunt still has validation errors - use advanced error commands.");
		}
	}

	private void prevError(AdventurerPlayer player, Adventure hunt)
	{
		ValidationError e = hunt.stepPrevValidationError();
		if(e!=null)
			player.sendMessage(e.getDescription());
		else
			player.sendMessage("Reached start of error list");
	}

	private void nextError(AdventurerPlayer player, Adventure hunt)
	{
		ValidationError e = hunt.stepNextValidationError();
		if(e!=null)
			player.sendMessage(e.getDescription());
		else
			player.sendMessage("Reached end of error list");
	}

	private void deleteError(AdventurerPlayer player, Adventure hunt)
	{
		ValidationError e = hunt.getCurrentError();
		hunt.remove(e.getHuntItem());
		player.sendMessage("Invalid item was removed, use /th validate to revalidate");
	}

	private void fixError(AdventurerPlayer player, Adventure hunt)
	{
		hunt.autoFixError();
		player.sendMessage("Attempted to fix error");
	}

	private void warpToError(AdventurerPlayer player,Adventure hunt)
	{
		ValidationError e = hunt.getCurrentError();
		
		if(e==null)
		{
			player.sendMessage("No current destination, use /th error next to step through errors");
			return;
		}
		
		Location l= e.getLocation();
		if(l==null)
		{
			player.sendMessage("Error has no associated location");
		}
		else
		{
			player.getPlayer().teleport(e.getLocation());
			player.sendMessage(e.getDescription());
		}
	}
}
