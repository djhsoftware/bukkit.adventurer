package com.computerosity.bukkit.adventure.datahandlers;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.UUID;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventureManager;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

//=============================================================
// Class       : Storage
//
// Description : storage handler
//
// Author      : Junkman
//=============================================================
public class MySqlDataHandler extends DataHandler
{
	private Connection connection;
	private AdventurerPlugin plugin;
	private String filename;
	private File sqlFile;

	public MySqlDataHandler(AdventurerPlugin thplugin, String server, String database, String username, String password)
	{
		type = HandlerType.MYSQL;
		plugin = thplugin;
		initDatabase();
	}

	public void resetDatabase()
	{
		executeDirect("drop table if exists jthhunt");
		executeDirect("drop table if exists jthhuntitem");
		executeDirect("drop table if exists jthhuntchest");
		executeDirect("drop table if exists jthplayer");
		executeDirect("drop table if exists jthplayerstats");
		executeDirect("drop table if exists jthplayerchest");
		executeDirect("drop table if exists jthplayerhunts");
		initDatabase();
	}

	public void initDatabase()
	{
		String query;

		// Create hunt table
		if (!TableExists("jthhunt"))
		{
			query = "CREATE TABLE jthhunt (h_huntid int NOT NULL, h_name varchar(45) NOT NULL, h_owner varchar(20) NOT NULL, h_secret varchar(6) NOT NULL, h_lastplayer VARCHAR(45) DEFAULT('') NOT NULL,";
			query += " h_sWorld varchar(20), h_sx double NULL, h_sy double NULL, h_sz double NULL, ";
			query += " h_pWorld varchar(20) NOT NULL, h_px double NOT NULL, h_py double NOT NULL, h_pz double NOT NULL, PRIMARY KEY (h_huntid))";
			executeDirect(query);
		}

		// Create huntitem table
		if (!TableExists("jthhuntitem"))
		{
			query = "CREATE TABLE jthhuntitem (hi_huntid INT NOT NULL, hi_itemid INT NOT NULL, hi_world varchar(20) NULL, hi_x double NULL, hi_y double NULL, hi_z double NULL, PRIMARY KEY (hi_huntid,hi_itemid))";
			executeDirect(query);
		}

		// Create huntchest table
		if (!TableExists("jthhuntchest"))
		{
			query = "CREATE TABLE jthhuntchest (hc_huntid INT NOT NULL, hc_typeid INT NULL, hc_amount INT NULL)";
			executeDirect(query);
		}

		// Create player table
		if (!TableExists("jthplayer"))
		{
			query = "CREATE TABLE jthplayer (p_playername varchar(20),p_activehuntid int, primary key (p_playername))";
			executeDirect(query);
		}

		// Create playerstats table
		if (!TableExists("jthplayerstats"))
		{
			query = "CREATE TABLE jthplayerstats (ps_playername varchar(20),ps_huntid int, ps_itemid int,ps_status int, primary key (ps_playername,ps_huntid,ps_itemid))";
			executeDirect(query);
		}

		// Create playerchests table
		if (!TableExists("jthplayerchest"))
		{
			query = "CREATE TABLE jthplayerchest (pc_playername varchar(20), pc_huntid int, pc_typeid int, pc_amount int)";
			executeDirect(query);
		}

		if (!TableExists("jthplayerhunts"))
		{
			query = "CREATE TABLE jthplayerhunts (ph_playername varchar(20), ph_huntid int, ph_found, ph_completed tinyint, PRIMARY KEY(ph_playername,ph_huntid))";
			executeDirect(query);
		}

		// Close connection
		closeConnection();
	}

	public void initConnection()
	{
		try
		{
			sqlFile = new File(plugin.getDataFolder(), filename);
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + sqlFile.getAbsolutePath());
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "SQLiteInit: exception");
		}
		catch (ClassNotFoundException e)
		{
			plugin.reportError(e, "SQLiteInit: class not found");
		}
	}

	public Connection getConnection()
	{
		if (connection == null) initConnection();
		return connection;
	}

	public void closeConnection()
	{
		if (connection == null) return;

		try
		{
			connection.close();
			connection = null;
			sqlFile = null;
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error closing connection");
		}
	}

	public boolean TableExists(String table)
	{
		try
		{
			Connection connection = getConnection();
			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet tables;
			tables = dbm.getTables(null, null, table, null);
			return (tables.next());
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "Error verifying database table: " + table);
		}
		return false;
	}

	private PreparedStatement createPreparedStatement(String query, Object... args)
	{
		try
		{
			int idx = 1;
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(query);
			for (Object obj : args)
			{
				if (obj instanceof String)
					statement.setString(idx++, (String) obj);
				else if (obj instanceof Double)
					statement.setDouble(idx++, (Double) obj);
				else if (obj instanceof Integer)
					statement.setInt(idx++, (Integer) obj);
				else if (obj == null)
					statement.setNull(idx++, java.sql.Types.INTEGER);
				else
					throw new Exception("Haven't catered for this data type in createPreparedStatement!");
			}

			return statement;
		}
		catch (Exception e)
		{
			plugin.reportError(e, "executeNonQuery: " + query);
			closeConnection();
			return null;
		}
	}

	public void executeInsert(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.executeUpdate();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeInsert: " + query);
			closeConnection();
		}
	}

	public void executeDelete(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.execute();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeDelete: " + query);
			closeConnection();
		}
	}

	public void executeUpdate(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
				executeDirect(query);
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				statement.executeUpdate();
				closeConnection();
			}
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeUpdate: " + query);
			closeConnection();
		}
	}

	private void executeDirect(String query)
	{
		try
		{
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			closeConnection();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeDirect: " + query);
			closeConnection();
		}
	}

	public ResultSet executeSelect(String query, Object... args)
	{
		try
		{
			if (args.length == 0)
			{
				Connection connection = getConnection();
				Statement statement = connection.createStatement();
				return statement.executeQuery(query);
			}
			else
			{
				PreparedStatement statement = createPreparedStatement(query, args);
				return statement.executeQuery();
			}
		}
		catch (Exception e)
		{
			plugin.reportError(e, "executeSelect: " + query);
			closeConnection();
			return null;
		}
	}

	public Integer executeScalarInt(String query, int defaultValue, Object... args) throws SQLException
	{
		Integer scalarResult = defaultValue;
		ResultSet result = null;
		try
		{
			result = executeSelect(query, args);
			if (result != null)
			{
				if (result.next()) scalarResult = result.getInt(1);
				result.close();
			}
			closeConnection();
		}
		catch (SQLException e)
		{
			plugin.reportError(e, "executeScalarInt: " + query, false);
		}
		finally
		{
			if (result != null) result.close();
			closeConnection();
		}

		return scalarResult;
	}

	@Override
	public boolean getIsPlayerRegistered(UUID uuid)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveHunt(Adventure hunt)
	{
		return false;
	}

	@Override
	public void loadHunts(AdventureManager hunts)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadHunt(Adventure hunt)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteHunt(Adventure hunt)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveAdventurer(AdventurerPlayer treasureHunter)
	{
	}
	
	@Override
	public AdventurerPlayer loadAdventurer(Player player)
	{
		return null;
	}

	@Override
	public void saveChestContents(UUID uuid, int huntid, ItemStack[] stacks)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkDatabaseVersion() {

	}

	@Override
	public void upgradeDatabaseVersion() {
		
	}
}
