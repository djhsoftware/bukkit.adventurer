package com.computerosity.bukkit.adventure;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import com.computerosity.bukkit.adventure.datahandlers.DataHandler;
import com.computerosity.bukkit.adventure.datahandlers.SqlLiteDataHandler;

// =============================================================
// Class       : Adventurer
//
// Description : Adventurer plugin
//
// Author      : Dave Hart
// =============================================================
public class AdventurerPlugin extends JavaPlugin
{
	private final Logger log = Logger.getLogger("Minecraft");

	@SuppressWarnings("unused")
	private AdventurerPlayerListener playerListener; 
	
	@SuppressWarnings("unused")
	private AdventurerBlockListener blockListener;
	public DataHandler dataHandler;
	public AdventureManager treasureHunts;
	public AdventurerManager treasureHunters;
	public boolean autorepair=false;
	
	@Override
	public void onEnable()
	{
		// Load configuration
		loadConfig();

		// Treasure hunt manager - managers hunts
		treasureHunts = new AdventureManager();
		
		// Treasure hunt manager - managers players and stats
		treasureHunters = new AdventurerManager(this);
		
		// Load treasure hunts
		dataHandler.loadHunts(treasureHunts);
		
		// Add currently connected players to hunt manager 
		treasureHunters.initialise(this.getServer().getOnlinePlayers());
		
		// Setup playerlistener
		playerListener = new AdventurerPlayerListener(this,treasureHunters);

		// Setup blockListener
		blockListener = new AdventurerBlockListener(this);
		/*

		// Setup scheduled data save
		//getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable()
		//{
		//    public void run()
		//    {
		//    	//System.out.println("Saving...");
		//		for(TreasureHuntPlayer hunter : treasureHunters) dataHandler.saveTreasureHunter(hunter);
		//    }
		//}, 1200L, 1200L);
	*/	
		// Register command
		getCommand("adv").setExecutor(new AdventurerCommand(this));

		// Write initialisation success
		PluginDescriptionFile pdfFile = this.getDescription();
		WriteToConsole("Adventurer Version " + pdfFile.getVersion() + " is enabled");
	}

	@Override
	public void onDisable()
	{
		// Save all hunts
		for(Adventure hunt : treasureHunts) 
		{
			// Sync back chest contents (see function for explanation)
			treasureHunters.syncChestContents(hunt.getPrize());
			
			// Save the hunt details
			dataHandler.saveHunt(hunt);
		}
		
		// Save all treasure hunters
		for(AdventurerPlayer hunter : treasureHunters)
		{
			dataHandler.saveAdventurer(hunter);
		}

		// Sync all chests back to DB
		WriteToConsole("plugin unloaded");
	}

	// Determines whether a player is able to call edit commands
	public void WriteToConsole(String message)
	{
		System.out.println("[Adventurer] " + message);
	}

	public void reportError(Exception e, String message)
	{
		reportError(e, message, true);
	}

	public void reportError(Exception e, String message, boolean dumpStackTrace)
	{
		PluginDescriptionFile pdfFile = this.getDescription();
		log.severe("[Adventurer " + pdfFile.getVersion() + "] " + message);
		if (dumpStackTrace) e.printStackTrace();
	}

	public void ReportWarning(String message)
	{
		PluginDescriptionFile pdfFile = this.getDescription();
		log.warning("[Adventurer " + pdfFile.getVersion() + "] " + message);
	}

	public void WriteEventLog(String message)
	{
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info("[" + pdfFile.getName() + " " + pdfFile.getVersion() + "] " + message);
	}

	public void loadConfig()
	{
		// Create data folder
		getDataFolder().mkdir();
		
		// Check for upgrade
		//CheckUpgrade();
		
		// Save default values to config.yml in datadirectory
		this.saveDefaultConfig();

		// Load the configuration
		FileConfiguration config = this.getConfig();

		// Get debug flag
		autorepair = config.getBoolean("options.autorepair",false);
		
		// Set up data handler
		String dhType = config.getString("storage.type","sqllite");
		String filename = config.getString("storage.filename",getDataFolder().getName() + ".db");

		if (dhType.equalsIgnoreCase("sqllite"))
		{
			dataHandler = new SqlLiteDataHandler(this, filename);
		}
		else if (dhType.equalsIgnoreCase("mysql"))
		{
			WriteToConsole("mysql is not implemented yet");
			//String server = config.getString("storage.server");
			//String database = config.getString("storage.database");
			//String username = config.getString("storage.username");
			//String password = config.getString("storage.password");
			//dataHandler = new MySqlDataHandler(this, server, database, username, password);
		}
		else
		{
			config.set("storage.type", "sqllite");
			WriteToConsole("Invalid storage type (should be sqllite,mysql) defaulting to sqllite");
			dataHandler = new SqlLiteDataHandler(this, filename);
		}
	}

	public void CheckUpgrade()
	{
		PluginDescriptionFile pdfFile = this.getDescription();
		String pluginVersion = pdfFile.getVersion();
		
		// Get configuration file
		FileConfiguration config = this.getConfig();

		// Get version from config file
		String version = config.getString("version","");
		if(!version.equalsIgnoreCase(pluginVersion))
		{
			WriteToConsole("Config: No upgrade path found for " + version + " --> " + pluginVersion);
		}
		
		// Check for database type change
		if(config.contains("newstorage"))
		{
			// TODO: Implement this...!
			// Allows migration from one storage type to another
		}
	}
}
