package com.computerosity.bukkit.adventure.command;

import org.bukkit.block.Block;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;
import com.computerosity.bukkit.adventure.huntitems.Clue;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /thstart commands
//
// Author      : Junkman
// =============================================================
public class CommandStart
{
	private final AdventurerPlugin plugin;

	public CommandStart(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public boolean execute(AdventurerPlayer player, String secret)
	{
		// Check permissions
		if (!player.isPermitted("start")) return true;

		Adventure hunt = null;

		// First, check if they're using a secret
		if (secret != "")
		{
			// See if they've put in a secret code
			hunt = plugin.treasureHunts.findHuntBySecret(secret);
			if (hunt == null)
			{
				player.sendMessage("Sorry that's not a valid secret code!");
				return true;
			}
		}
		else
		{
			// Otherwise see what block they're looking at
			Block block = player.getPlayer().getTargetBlock(null, 3);
			Clue item = plugin.treasureHunts.findClueByLocation(block.getLocation());
			if (item != null)
			{
				if (!item.getIsStart())
				{
					player.sendMessage("You'll need to find the start point first!");
					return true;
				}
			}
			else
			{
				player.sendMessage("You'll need to find a secret or start sign first!");
				return true;
			}
			hunt = item.getParent();
		}

		// Have we already completed this hunt?
		if (!player.getIsHuntCompleted(hunt))
		{
			// Start the hunt!
			player.startTreasureHunt(hunt);
		}
		else
		{
			player.sendMessage("You've already completed this treasure hunt!");
		}

		return true;
	}
}
