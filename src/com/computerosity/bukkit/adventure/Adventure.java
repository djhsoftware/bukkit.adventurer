package com.computerosity.bukkit.adventure;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.HuntItem;
import com.computerosity.bukkit.adventure.huntitems.ItemType;
import com.computerosity.bukkit.adventure.huntitems.Prize;

public class Adventure extends ArrayList<HuntItem>
{	
	private static final long serialVersionUID = 7460331911186902180L;
	                                             
	private String name;
	private UUID owner;
	private String secret = "";
	private Clue start=null;
	private Prize prize = null;
	private int id;
	private boolean isValid=true;
	private AdventurerPlayer editedBy=null;
	private ArrayList<ValidationError> validationErrors= new ArrayList<ValidationError>();
	private int currentErrorIndex = -1;
	
	public ArrayList<Clue> clues = new ArrayList<Clue>();

	public Adventure(UUID _owner)
	{
		id = generateUID();
		name = "Treasure Hunt " + id;
		owner = _owner;
		setSecret("");
	}
	
	public Adventure(int _id)
	{
		id = _id;
	}

	public HuntItem findItemByLocation(Location location)
	{
		if(start!=null && start.hasLocation(location)) return start;
		if(prize!=null && prize.hasLocation(location)) return prize;
		
		for(Clue item : this.clues)
		{
			// Check location
			if (item.hasLocation(location)) return item;
		}

		return null;
	}

	public Clue findClueByLocation(Location location)
	{
		if (start != null && start.hasLocation(location)) return start;

		for(Clue clue : this.clues)
		{
			if (clue.hasLocation(location)) return clue;
		}

		return null;
	}
	
	public Clue findClueById(int id)
	{
		for(Clue clue : this.clues)
		{
			if(clue.getId()==id) return clue;
		}	
		return null;
	}
	
	public void setStartClue(Clue clue)
	{
		// Replace existing start item
		if (start != null)
		{
			// Add the existing start item to the clue list
			// and reset start flag
			clues.add(start);
			start.setIsStart(false);
		}

		// Remove this clue from the clue list and set as start
		start = clue;
		start.setIsStart(true);
		clues.remove(start);
	}
	
	public int removeItem(Clue itm)
	{
		/*
		 * 0=Removed OK 1=Relocated 2=Couldn't relocate
		 */
		int code = 0;
		if (start != null && start.equals(itm))
		{
			code = 2;
			// Attempt to find new start point
			for(Clue clue : this.clues)
			{
				if (!clue.equals(itm))
				{
					setStartClue(clue);
					code = 1;
					break;
				}
			}
		}

		// Remove the node
		if (start.equals(itm)) start = null;
		clues.remove(itm);

		return code;
	}

	public int generateUID()
	{
		boolean uidOK = false;
		int uid = 0;
		int attempts = 10;

		Random rng = new Random();

		while (!uidOK && attempts > 0)
		{
			attempts--;
			uidOK = true;
			uid = rng.nextInt(2000);

			for(Clue clue : clues)
			{
				if (clue.getItemId() == uid) uidOK = false;
			}
		}

		return uid;
	}

	public String getName()
	{
		return name;
	}

	public String getSecret()
	{
		return secret;
	}

	public void setName(String string)
	{
		name = string;
		ResetSigns();
	}

	public void setOwner(UUID uuid)
	{
		owner = uuid;
	}

	public void setSecret(String string)
	{
		if(string=="")
            secret = GenerateRandomSecret();
        else
            secret = string;		
		ResetSigns();
	}

	private void ResetSigns()
	{
		for(Clue clue : clues)
		{
			clue.setSignText();
		}	
	}
	
	public void setPrize(Prize _prize)
	{
		prize = _prize;		
	}

	public Prize getPrize()
	{
		return prize;
	}

	public boolean hasStart()
	{
		return (start!=null);
	}

	public int getClueCount()
    {
        return clues.size();
    }

	public int getID()
	{
		return this.id;
	}

	public void invalidate(String string)
	{
		isValid = false;
		// TODO: NOTIFY
	}

	public boolean getIsValid()
	{
		return isValid;
	}

	public void startEditing(AdventurerPlayer treasureHuntPlayer)
	{
		editedBy = treasureHuntPlayer;
	}
	
	public void endEditing()
	{
		editedBy = null;
	}
	
	public AdventurerPlayer getEditedBy()
	{
		return editedBy;
	}

	public UUID getOwner()
	{
		return owner;
	}
	
	private String GenerateRandomSecret()
    {
        int length = 6;
        Random rng = new Random();
        //String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String characters = "ABCDEFGHIJKLMNOPRSTWXYZ"; // Some letters are hard to distinguish...
        char[] text = new char[length];

        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }

        return new String(text);
    }
	
	// Scan the physical properties of a hunt 
	// to ensure the world still matches the hunt configuration
	public boolean validate()
	{
		// Clear existing errors
		validationErrors.clear();
		currentErrorIndex=0;
		isValid = true;
		
		// First, check the prize chest exists
		if(prize!=null)
		{
			if(prize.getBlock().getType()!=Material.CHEST)
			{
				String errtext = prize.getType() + "(" + prize.getId() + ") @ " + prize.location + ": world block is " + prize.getBlock().getType() + " expecting CHEST";
				validationErrors.add(new ValidationError(this,prize,errtext));
				isValid=false;
			}
		}
		else
		{
			validationErrors.add(new ValidationError(this,null,"Hunt has no prize!"));
			isValid=false;
		}

		// Check start clue
		if(start!=null)
		{
			if(start.getBlock().getType()!=Material.WALL_SIGN && 
			   start.getBlock().getType()!=Material.SIGN_POST)
			{
				String errtext = start.getType() + "(" + start.getId() + ") @ " + start.location + ": world block is " + start.getBlock().getType() + " expecting SIGN|SIGN_POST";
				validationErrors.add(new ValidationError(this,start,errtext));
				isValid=false;
			}
			else
				start.setSignText();
		}
		else
		{
			validationErrors.add(new ValidationError(this,null,"Hunt has no start point!"));
			isValid=false;
		}
		
		// Check all clues
		for(Clue clue : clues)
		{
			if(clue.getBlock().getType()!=Material.WALL_SIGN && 
			   clue.getBlock().getType()!=Material.SIGN_POST)
			{
				String errtext = clue.getType() + "(" + clue.getId() + ") @ " + clue.location + ": world block is " + clue.getBlock().getType() + " expecting SIGN|SIGN_POST";
				validationErrors.add(new ValidationError(this,clue,errtext));
				isValid=false;
			}
			else
				clue.setSignText();
		}
	
		return isValid;
	}

	public boolean repair()
	{
		this.validate();
		ValidationError err = this.getFirstError();
		while(err!=null) 
		{
			autoFixError();
			err = this.stepNextValidationError();
		}
		return (this.validate());
	}
	
	private ValidationError getFirstError()
	{
		currentErrorIndex = 0;
		if(validationErrors.size()==0) 
			return null;
		else 
			return validationErrors.get(currentErrorIndex);
	}
	
	public void autoFixError()
	{
		ValidationError e = this.getCurrentError();
		Block block = e.getLocation().getBlock();
		HuntItem item = e.getHuntItem();
		if(item.getType()==ItemType.CLUE || item.getType()==ItemType.START)
		{
			// if there's a wall next to the sign post use as a wall sign, otherwise
			// use as a floor sign (68 = WALL_SIGN)
			if(block.getRelative(BlockFace.NORTH).getType()!=Material.AIR)
				block.setTypeIdAndData(68,(byte) 0x5,true); 
			else if(block.getRelative(BlockFace.SOUTH).getType()!=Material.AIR)
				block.setTypeIdAndData(68,(byte) 0x4,true); 
			else if(block.getRelative(BlockFace.WEST).getType()!=Material.AIR)
				block.setTypeIdAndData(68,(byte) 0x2,true); 
			else if(block.getRelative(BlockFace.EAST).getType()!=Material.AIR)
				block.setTypeIdAndData(68,(byte) 0x3,true); 
			else
				block.setType(Material.SIGN_POST);
			
			Clue clue = (Clue)item;			
			clue.setSignText();
		}
		else if(item.getType()==ItemType.PRIZE)
		{
			// TODO: Fix double chests???
			block.setType(Material.CHEST);
		}
	}

	public void showValidationErrors(AdventurerPlayer player)
	{
		for(ValidationError err : validationErrors)
		{
			player.sendMessage(err.getDescription());
		}
	}
	
	public ValidationError stepNextValidationError()
	{
		if(currentErrorIndex >= validationErrors.size()-1) return null;
		return validationErrors.get(++currentErrorIndex);
	}

	public ValidationError stepPrevValidationError()
	{
		if(validationErrors.size()==0 || currentErrorIndex == 0) return null;
		return validationErrors.get(--currentErrorIndex);
	}

	public ValidationError getCurrentError()
	{
		if(validationErrors.size()==0) return null;
		if(currentErrorIndex==-1) currentErrorIndex=0;
		return validationErrors.get(currentErrorIndex);
	}

	public Clue getStart()
	{
		return start;
	}
}
