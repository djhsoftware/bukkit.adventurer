package com.computerosity.bukkit.adventure;

import java.util.ArrayList;
import org.bukkit.Location;

import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.HuntItem;
import com.computerosity.bukkit.adventure.huntitems.Prize;

public class AdventureManager extends ArrayList<Adventure>
{
	private static final long serialVersionUID = -1612432486489371117L;

	public Adventure findHuntByLocation(Location l)
	{
		HuntItem found = null;

		for (Adventure hunt : this)
		{
			found = hunt.findItemByLocation(l);
			if (found != null) return hunt;
		}

		return null;
	}

	public Adventure findHuntByID(int id)
	{
		for (Adventure hunt : this)
		{
			if(hunt.getID()==id) return hunt;
		}

		return null;
	}

	public Clue findClueByLocation(Location location)
	{
		Clue found = null;

		for (Adventure hunt : this)
		{
			found = hunt.findClueByLocation(location);
			if (found != null) return found;
		}

		return null;
	}

	public HuntItem findItemByLocation(Location location)
	{
		HuntItem found = null;

		for (Adventure hunt : this)
		{
			found = hunt.findItemByLocation(location);
			if (found != null) return found;
		}
		
		return null;
	}

	public Prize findPrizeByLocation(Location location)
	{
		for (Adventure hunt : this)
		{
			Prize prize = hunt.getPrize();
			if(prize.hasLocation(location)) return prize;
		}

		return null;
	}

	public Adventure findHuntBySecret(String secret)
	{
		for (Adventure hunt : this)
		{
			if (hunt.getSecret().equalsIgnoreCase(secret)) return hunt;
		}

		return null;
	}

	public Adventure findHuntByName(String arg)
	{
		for (Adventure hunt : this)
		{
			if (hunt.getName().equalsIgnoreCase(arg)) return hunt;
		}

		return null;
	}
}
