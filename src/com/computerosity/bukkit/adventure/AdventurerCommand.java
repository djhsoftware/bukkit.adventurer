package com.computerosity.bukkit.adventure;

import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.computerosity.bukkit.adventure.command.*;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /adv commands
//
// Author      : Junkman
// =============================================================
public class AdventurerCommand implements CommandExecutor
{
	private final AdventurerPlugin plugin;
	private final CommandNew commandNew;
	private final CommandList commandList;
	private final CommandSave commandSave;
	private final CommandLoad commandLoad;
	private final CommandValidate commandValidate;
	private final CommandSet commandSet;
	private final CommandUnset commandUnset;
	private final CommandEnd commandEnd;
	private final CommandDelete commandDelete;
	private final CommandInfo commandInfo;
	private final CommandHelp commandHelp;
	private final CommandStart commandStart;
	private final CommandEdit commandEdit;
	private final CommandError commandError;
	private final CommandClue commandClue;
	
	public AdventurerCommand(AdventurerPlugin plugin)
	{
		this.plugin = plugin;

		commandNew = new CommandNew(plugin);
		commandList = new CommandList(plugin);
		commandSave = new CommandSave(plugin);
		commandLoad = new CommandLoad(plugin);
		commandValidate = new CommandValidate(plugin);
		commandSet = new CommandSet(plugin);
		commandUnset = new CommandUnset(plugin);
		commandEnd = new CommandEnd(plugin);
		commandDelete = new CommandDelete(plugin);
		commandInfo = new CommandInfo(plugin);
		commandHelp = new CommandHelp(plugin);
		commandStart = new CommandStart(plugin);
		commandEdit = new CommandEdit(plugin);
		commandError = new CommandError(plugin);
		commandClue = new CommandClue(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] split)
	{
		if (!(sender instanceof Player)) return false;
		Player player = (Player) sender;

		// Locate/create treasure hunt player
		AdventurerPlayer p = plugin.treasureHunters.registerPlayer(player);
		
		if (split.length == 0)
			commandHelp.execute(p, "");
		else
		{
			String token = split[0];

			// Splurge the remaining bits together
			String arg = "";
			if (split.length > 1)
			{
				for (int i = 1; i < split.length; i++)
					arg = arg + split[i] + " ";
				arg = arg.trim();
			}

			// Create a new treasure hunt
			if (token.equalsIgnoreCase("new"))
				commandNew.execute(p, arg);

			// Edit a hunt
			else if (token.equalsIgnoreCase("edit"))
				commandEdit.execute(p, arg);
			
			// List all hunts available to you
			else if (token.equalsIgnoreCase("list"))
				commandList.execute(p, arg);

			// Cancel editing - not yet supported, 
			// maybe after export is implemented then export before edit..
			//else if (token.equalsIgnoreCase("cancel"))
			//	commandCancel.execute(p, arg);
			
			// Save the currently edited hunt
			else if (token.equalsIgnoreCase("save"))
				commandSave.execute(p, arg);

			// Load a hunt
			else if (token.equalsIgnoreCase("load"))
				commandLoad.execute(p, arg);

			// End editing of the hunt
			else if (token.equalsIgnoreCase("end"))
				commandEnd.execute(p, arg);

			// Set a configuration option on a hunt or clue
			else if (token.equalsIgnoreCase("set"))
				commandSet.execute(p, arg);

			// Set a configuration option on a hunt or clue
			else if (token.equalsIgnoreCase("unset"))
				commandUnset.execute(p, arg);

			// End editing & Save
			else if (token.equalsIgnoreCase("end"))
				commandEnd.execute(p, arg);

			// Delete the given hunt
			else if (token.equalsIgnoreCase("delete"))
				commandDelete.execute(p, arg);

			// Get information
			else if (token.equalsIgnoreCase("info"))
				commandInfo.execute(p, arg);

			// Start a treasure hunt
			else if (token.equalsIgnoreCase("start"))
				commandStart.execute(p, arg);

			// Mark a hunt item (/adv set clue)
			else if (token.equalsIgnoreCase("mark"))
				commandSet.execute(p,"clue " + arg);

			// Unmark a hunt item (/adv unset clue)
			else if (token.equalsIgnoreCase("unmark"))
				commandUnset.execute(p,"clue");

			// Validate a hunt
			else if (token.equalsIgnoreCase("validate"))
				commandValidate.execute(p,arg);
			
			// Validation error handling commands
			else if (token.equalsIgnoreCase("error"))
				commandError.execute(p,arg);

			// Validation error handling commands
			else if (token.equalsIgnoreCase("clue"))
				commandClue.execute(p,arg);
			
			// Show help
			else
				commandHelp.execute(p, arg);
		}

		// Location location = player.getLocation();

		return true;
	}
}
