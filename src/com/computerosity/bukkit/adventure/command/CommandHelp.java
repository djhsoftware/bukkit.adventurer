package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /adv help
//
// Author      : Dave Hart
// =============================================================
public class CommandHelp
{
	public CommandHelp(AdventurerPlugin plugin)
	{
	}

	public boolean execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!player.isPermitted("help")) return true;

		if (!player.getIsEditing())
		{
			player.sendMessage("Junks treasure hunt - help:");
			player.sendMessage("/adv start - Start a treasure hunt (needs secret code)");
			player.sendMessage("/adv clue - Gives a clue for the current treasure hunt");
			player.sendMessage("/adv end - Stops the current hunt (you can resume later)");
			player.sendMessage("/adv info - Gives information about treasure hunts");
			player.sendMessage("/adv new - Create new treasure hunt");
		}
		else
		{
			player.sendMessage("Junks treasure hunt - editing help:");
			player.sendMessage("/adv set [secret|prize|name|owner|clue|start]");
			player.sendMessage("/adv unset [prize|clue|start]");
			player.sendMessage("/adv mark - equivalent to /adv set clue");
			player.sendMessage("/adv unmark - equivalent to /adv unset clue");
			//player.sendMessage("/adv delete|undelete - Mark/unmark hunt for deletion (deleted on end)");
			player.sendMessage("/adv validate - Check the edited hunt for errors");
			player.sendMessage("/adv save - Save changes to current hunt");
			player.sendMessage("/adv end - Stop editing and save changes");
		}

		return true;
	}
}
