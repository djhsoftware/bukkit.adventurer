package com.computerosity.bukkit.adventure;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

// =============================================================
// Class       : TreasureHuntBlockListener
//
// Description : Block event listener
//
// Author      : Junkman
// =============================================================
public class AdventurerBlockListener implements Listener
{
	private final AdventurerPlugin plugin;

	public AdventurerBlockListener(final AdventurerPlugin _plugin)
	{
		this.plugin = _plugin;
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockDamage(BlockDamageEvent event)
	{
		// Return if not enabled
		if (!plugin.isEnabled()) return;

		if (IsPartOfHunt(event.getBlock(), event.getPlayer())) event.setCancelled(true);
	}

    @EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event)
	{
		// Return if not enabled
		if (!plugin.isEnabled()) return;

		if (IsPartOfHunt(event.getBlock(), event.getPlayer())) event.setCancelled(true);
	}

    @EventHandler(priority = EventPriority.NORMAL)
	public void onSignChange(SignChangeEvent event)
	{
		// Return if not enabled
		if (!plugin.isEnabled()) return;

		if (IsPartOfHunt(event.getBlock(), event.getPlayer())) event.setCancelled(true);
	}

	// Check if block is a chest and is in our list of chests
	// If so don't allow it to be destroyed
	// If not on a hunt: You can't delete this block, it's part of a
	// treasure hunt
	// If on a hunt and this is on another: You can't open this chest, it's
	// part of another treasure hunt
	// If on the hunt but it's not in sequence: You can't open this chest
	// yet - find all the clues!
	private boolean IsPartOfHunt(Block block, Player player)
	{
		// If th block is not a sign or chest it's not part of a hunt
		Material type = block.getType();
		if (type != Material.SIGN_POST && type != Material.WALL_SIGN && type != Material.CHEST) return false;

		// If so, check against our list...
		Location location = block.getLocation();
		if (plugin.treasureHunts.findHuntByLocation(location) != null)
		{
			player.sendMessage("This is part of a treasure hunt and is magically protected");
			return true;
		}
		else
			return false;
	}
}