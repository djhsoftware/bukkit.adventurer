package com.computerosity.bukkit.adventure.command;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;
import com.computerosity.bukkit.adventure.huntitems.Clue;

public class CommandUnset
{
	private final AdventurerPlugin plugin;

	public CommandUnset(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public void execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!player.isPermitted("unset")) return;

		if (arg.equalsIgnoreCase("prize"))
		{
			unsetPrize(player);
		}
		else if (arg.equalsIgnoreCase("clue"))
		{
			unsetClue(player);
		}
		else if (arg.equalsIgnoreCase("delete"))
		{
			unsetStart(player);
		}
		else
			player.sendMessage("Syntax: /th unset [prize|clue|start|delete]");
	}

	private void unsetPrize(AdventurerPlayer player)
	{
		// TODO Auto-generated method stub
		
	}

	private void unsetClue(AdventurerPlayer player)
	{
		//if (!player.isPermitted("unset.clue")) return;
		
		Adventure edithunt = player.getEditedHunt();
		
		// Are we editing?
		if (edithunt==null)
		{
			player.sendMessage("You are not editing a hunt");
			return;
		}

		Player p = player.getPlayer();
		Block block = p.getTargetBlock(null, 3);
		if (block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN || block.getType() == Material.CHEST)
		{
			Location location = block.getLocation();

			// First, check to see if ths block is already part of a hunt
			Adventure hunt = plugin.treasureHunts.findHuntByLocation(location);
			if (hunt != null)
			{
				// If it is - is it part of the hunt we're editing?
				if (hunt.equals(edithunt))
				{
					if (block.getType() == Material.CHEST)
					{
						player.sendMessage("You cannot unmark chests, to release the chest delete the hunt");
					}
					else
					{
						Clue itm = plugin.treasureHunts.findClueByLocation(location);
						int code = hunt.removeItem(itm);
						switch (code)
						{
							case 0: // It wasn't a start point
								player.sendMessage("Item has been unmarked and is no longer part of the treasure hunt");
								break;
							case 1: // It was a start point and we relocated
								player.sendMessage("Item has been unmarked, a new start point was assigned");
								break;
							case 2: // It was a start point and we couldn't
									// relocate
								player.sendMessage("Item has been unmarked, this hunt has no start point!");
								break;
						}
					}
				}
				else
				{
					// It's in another hunt - we can't add it
					player.sendMessage("This item is part of another treasure hunt");
				}
			}
			else
			{
				// It's not part of a treasure hunt at all
				player.sendMessage("This item is not in any treasure hunt");
			}
		}
	}

	private void unsetStart(AdventurerPlayer player)
	{
		// TODO Auto-generated method stub
		
	}
}
