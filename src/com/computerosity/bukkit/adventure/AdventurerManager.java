package com.computerosity.bukkit.adventure;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.computerosity.bukkit.adventure.huntitems.Prize;

public class AdventurerManager extends ArrayList<AdventurerPlayer>
{
	private static final long serialVersionUID = 8045441345759660260L;
	private AdventurerPlugin plugin=null;
	
	public AdventurerManager(AdventurerPlugin _plugin)
	{
		plugin = _plugin;
	}
	
	public AdventurerPlayer registerPlayer(Player player)
	{
		// Does it already exist, if so just return it
		AdventurerPlayer hunter = find(player.getUniqueId());
		if(hunter!=null) return hunter;
		
		// If not, create it then return it
		plugin.WriteToConsole("Register: "+player.getName());
		hunter = new AdventurerPlayer(player);
		this.add(hunter);
		return hunter;
	}

	private AdventurerPlayer find(UUID uuid)
	{
		for(AdventurerPlayer hunter : this) 
			if(hunter.getUniqueId()==uuid)
				return hunter;
		
		return null;
	}

	// When logging in, connect the player to the hunt manager
	public void onPlayerLogin(Player player)
	{
		// Attempt to load the player from database
		AdventurerPlayer p = plugin.dataHandler.loadAdventurer(player);
		if(p!=null)
		{
			plugin.WriteToConsole("Connect: "+p.getName());
			this.add(p);
		}
	}

	// When disconnecting disconnect the player from the hunt manager
	public void onPlayerQuit(Player player)
	{
		// Save player to database
		AdventurerPlayer p = find(player.getUniqueId());
		if(p!=null)
		{
			plugin.dataHandler.saveAdventurer(p);
			plugin.WriteToConsole("Disconnect: "+p.getName());
			this.remove(p);
		}
	}

	// When starting up, connect all current players to the hunt manager
	public void initialise(Player[] onlinePlayers)
	{
		for(Player p : onlinePlayers)
		{
			plugin.WriteToConsole("Init: "+p.getUniqueId());
			onPlayerLogin(p);
		}
	}
	
	public void save()
	{
		for(AdventurerPlayer hunter : this) 
			plugin.dataHandler.saveAdventurer(hunter);
	}
	
	// This is a fix for the lack of inventory_close event.
	// When the chest is opened we have to move any
	// contents of the chest to the last accessing player inventory
	// If the player is not currently online it writes directly to the database
	public void syncChestContents(Prize prize)
	{
		if(prize==null) return;
		
		// Is a last player registered?
		UUID lastPlayer = prize.getLastPlayer();
		if(lastPlayer==null) return;
		
		//plugin.WriteToConsole("Sync back chest -> '" + lastPlayer + "'");
		AdventurerPlayer p = find(lastPlayer);
		
		if(p!=null)
		{
			// Player is online - copy to the online inventory
			AdventurerPlayerQuest tph = p.findByQuest(prize.getParent());
			assert(tph!=null) : "syncChestContents:tph is null";
			if(tph!=null) tph.updatePlayerChestFromWorld();
		}
		else
		{
			// Player is offline - write to database
			plugin.dataHandler.saveChestContents(lastPlayer, prize.getParent().getID(), prize.getChestContents());			
		}
	}
}
