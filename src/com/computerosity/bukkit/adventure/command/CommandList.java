package com.computerosity.bukkit.adventure.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

public class CommandList {
	private final AdventurerPlugin plugin;

	public CommandList(AdventurerPlugin plugin) {
		this.plugin = plugin;
	}

	public void execute(AdventurerPlayer player, String arg) {
		if (arg.isEmpty()) {
			list(player);
		} else if (arg.equalsIgnoreCase("all")) {
			listAll(player);
		} else {
			// listPlayer();
		}
	}

	private void list(AdventurerPlayer player) {
		// Check permissions
		if (!player.isPermitted("list.own"))
			return;

		player.sendMessage("Owned treasure hunts:");
		for (Adventure hunt : plugin.treasureHunts) {
			if (hunt.getOwner() == player.getUniqueId()) {
				player.sendMessage(hunt.getName());
			}
		}
	}

	private void listAll(AdventurerPlayer player) {
		// Check permissions
		if (!player.isPermitted("list.all"))
			return;

		player.sendMessage("All treasure hunts:");
		for (Adventure hunt : plugin.treasureHunts) {
			
			String playerName = "Unknown";
			Player p = Bukkit.getPlayer(hunt.getOwner());
			if (p != null)
				playerName = p.getName();

			String huntName = hunt.getName();
			if (huntName.length() > 30)
				huntName = huntName.substring(0, 27) + "...";

			String editing = "";
			if (hunt.getEditedBy() != null)
				editing = "(editing)";
			
			player.sendMessage(String.format("%-20s %-30s %s", playerName, huntName, editing));
		}
	}
}
