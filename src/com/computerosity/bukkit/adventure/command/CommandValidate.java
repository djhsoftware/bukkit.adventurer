package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

public class CommandValidate
{
	private final AdventurerPlugin plugin;

	public CommandValidate(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public void execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!(player.isPermitted("owner.validate") || player.isPermitted("validate"))) return;
		
		Adventure hunt=null;
		
		// If we're editing and no hunt is specified, validate the edited hunt
		if(player.isEditing() && arg.length()==0)
		{
			hunt = player.getEditedHunt();
		}
		else
			hunt = plugin.treasureHunts.findHuntByName(arg);
		
		if(hunt==null)
		{
			player.sendMessage("No hunt was found with that name");
			return;
		}
		
		// Check for hunt editing permissions
		if(hunt.getOwner()!=player.getUniqueId() && !player.isPermitted("validate"))
		{
			player.sendMessage("You are only permitted to validate your own hunts");
			return;
		}

		if(hunt.validate())
		{
			player.sendMessage("Hunt passed validation");
		}
		else
		{
			player.sendMessage("Hunt validation failed:");
			hunt.showValidationErrors(player);
		}
	}
}
