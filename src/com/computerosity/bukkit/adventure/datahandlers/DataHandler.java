package com.computerosity.bukkit.adventure.datahandlers;

import java.sql.Connection;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.computerosity.bukkit.adventure.Adventure;
import com.computerosity.bukkit.adventure.AdventureManager;
import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

public abstract class DataHandler
{
	protected HandlerType type;
	protected Connection connection;
	protected AdventurerPlugin plugin;

	public HandlerType getType()
	{
		return type;
	}

	public abstract void resetDatabase();
	public abstract void initDatabase();
	public abstract void checkDatabaseVersion();
	public abstract void upgradeDatabaseVersion();
	public abstract boolean getIsPlayerRegistered(UUID uuid);

	public abstract void loadHunts(AdventureManager hunts);
	public abstract boolean saveHunt(Adventure hunt);
	public abstract void loadHunt(Adventure hunt);
	public abstract void deleteHunt(Adventure hunt);
	public abstract void saveAdventurer(AdventurerPlayer treasureHunter);
	public abstract AdventurerPlayer loadAdventurer(Player player);
	public abstract void saveChestContents(UUID uuid, int huntid, ItemStack[] stacks);
}
