package com.computerosity.bukkit.adventure;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.computerosity.bukkit.adventure.datahandlers.DataHandler;
import com.computerosity.bukkit.adventure.huntitems.Clue;
import com.computerosity.bukkit.adventure.huntitems.Prize;

public class AdventurerPlayer
{
	private Player player=null;
	private AdventurerPlayerQuest activeQuest = null;
	public ArrayList<AdventurerPlayerQuest> hunts = new ArrayList<AdventurerPlayerQuest>(); 
	private Adventure edited=null;
	
	public AdventurerPlayerQuest getActiveHunt()
	{
		return activeQuest;
	}
	
	public String getName()
	{
		return player.getName();
	}

	public UUID getUniqueId() {
		return player.getUniqueId();
	}

	public void setPlayer(Player _player)
	{
		player = _player;
	}
	
	public AdventurerPlayer(Player _player)
	{
		player = _player;
	}

	private void setupHuntStats(Adventure hunt)
	{
		// First, search for the hunt
		activeQuest = findOrCreateHunt(hunt);
	}
	
	public AdventurerPlayerQuest findByQuest(Adventure hunt)
	{
		// First, search for the hunt
		for(AdventurerPlayerQuest pHunt : hunts)
			if(pHunt.getHunt()==hunt) return pHunt;
		
		return null;
	}

	private AdventurerPlayerQuest findOrCreateHunt(Adventure quest)
	{
		// First, search for the hunt
		AdventurerPlayerQuest pquest = findByQuest(quest);

		// If it doesn't exist, create it
		if(pquest==null)
		{
			pquest = new AdventurerPlayerQuest(this,quest);
			hunts.add(pquest);
		}
		
		return pquest;
	}
	
	public boolean onClueInteract(Clue clue)
	{
		boolean statusChanged = false;
		
		String pMessage = "This is part of a quest and is magically protected";

		if(activeQuest==null)
		{
			// Already completed this hunt?
			AdventurerPlayerQuest ph = findByQuest(clue.getParent());
			if(ph!=null && ph.getIsComplete())
			{
				pMessage = "You've already completed this quest!";
			}
			else if(clue.getIsStart()) // If it's a start sign and we're not on a hunt, start the hunt
			{
				Adventure hunt = clue.getParent();
				setupHuntStats(hunt);
		        player.sendMessage("You've discovered a new quest!");
		        player.sendMessage("'" + hunt.getName() + "'");
		        
		        int count = hunt.getClueCount();
		        switch(count)
		        {
		            case 0:
		                player.sendMessage("There are no other items to find - go for the chest!");
		                break;
		            case 1:
		                player.sendMessage("There is one more item to find!");
		                break;
		            default:
		                player.sendMessage("There are " + count + " more items to find!");
		                break;
		        }
		        
		        // We've change status, need to save...
		        statusChanged=true;
			}
			else
			{
				pMessage = "You need to find the start point to embark on this quest";
			}
		}
		else // Currently on a treasure hunt?
		{
			// Is this the hunt they're currently on?
			if (activeQuest.getHunt()==clue.getParent())
			{
				activeQuest.markClueFound(clue);
				
				if (activeQuest.allCluesFound())
				{
					pMessage = "You've found all the items, the secret chest is unlocked!";
				}
				else
					pMessage = "You found a treasure hunt item!";
		        
		        // We've change status, need to save...
		        statusChanged=true;
			}
			else
				pMessage = "Sorry, this item is part of another treasure hunt!";
		}

		// Display message
		player.sendMessage(pMessage);
		
		return statusChanged;
	}
	
	public boolean onPrizeInteract(Prize prize)
	{
		// Set default message
		String pMessage = "This is part of a treasure hunt and is magically protected";

		if(prize==null) return false;
		
		Adventure hunt = prize.getParent();
		AdventurerPlayerQuest phunt = findOrCreateHunt(hunt);

		// Is anyone editing this hunt?
		if(hunt.getEditedBy()!=null)
		{
			// Is the player editing this hunt?
			if(edited.equals(hunt))
			{
				prize.updateWorldFromContents();
				return false;
			}
			else
			{
				pMessage = "This hunt is currently being edited - come back later";
				return true;
			}
		}

		// Does the hunt have a start point? If not this is just a magic chest
		if (!prize.getParent().hasStart())
		{
			// Now fill it with default contents
			if(prize.getChest()!=null) stockChestFromPlayer(phunt);

			// Let them open it
			return false;
		}

		// TODO: There's a bit of shitty here, in that if you find the chest
		// before finding all the items it won't broadcast your success on
		// completing the hunt...
		// This is because it sets the prize found status the first time you
		// find the chest

		// If they've already completed this hunt, allow access
		if (phunt.getIsComplete())
		{
			// Fill with the items they left behind when they opened the chest
			stockChestFromPlayer(phunt);
			
			player.sendMessage("Welcome back, treasure hunter!");
			return false;
		}

		// Are we currently on a treasure hunt?
		if(activeQuest==null)
		{
			// Don't allow access to the chest
			player.sendMessage("This is part of a treasure hunt and is magically protected");
			return true;
		}
		
		// Are we currently on the treasure hunt for which this is the prize?
		if (activeQuest.getHunt()==hunt)
		{
			// Found all the clues or there are no clues?
			if (activeQuest.allCluesFound())// || activeHunt.getHunt().getClueCount() == 0)
			{
				// Set hunt as completed
				activeQuest.setIsComplete(true);

				// Broadcast to the world!!
				// TODO: Set this as configuration item
				//Server s = plugin.getServer();
				//s.broadcastMessage(player.getDisplayName() + " has completed '" + hunt.getName() + "'!");

				// Fill the player inventory with the chest items
				activeQuest.stockPlayerChestFromHunt();

				// Fill the chest with the player contents
				activeQuest.stockWorldChestFromPlayer();

				// End the treasure hunt
				clearActiveHunt();

				// Let them open the chest and reap the rewards!
				return false;
			}
			else
				pMessage = "You have not completed the treasure hunt yet!";
		}
		else 
			pMessage = "This is part of another treasure hunt!";

		// Access denied, show the reason and return 'cancel'
		player.sendMessage(pMessage);
		return true;
	}

	private void stockChestFromPlayer(AdventurerPlayerQuest phunt)
	{
		// Sync contents from player
		phunt.stockWorldChestFromPlayer();

		// FIX for inventory close
		phunt.getHunt().getPrize().setLastPlayer(player.getUniqueId());
	}

	public boolean isPermitted(String permission)
	{
		String fullPermission = "treasurehunt." + permission;
//		String ownerPermission = "owner." + permission;
		
		// Full permissions?
		if (player.hasPermission(fullPermission)) return true;

/*		// Nothing else applies unless we're editing
		if (plugin.edited != null)
		{

			if (permissions.hasPermission(player, ownerPermission, true) && plugin.edited.getOwner().equalsIgnoreCase(player.getName()))
			{
				return true;
			}
			else
			{
				player.sendMessage(ChatColor.RED + "You are only allowed to run this command on your own hunts");
				return false;
			}
		}*/

		player.sendMessage(ChatColor.RED + "You are not allowed to use this command");
		return false;
	}

	public Player getPlayer()
	{
		return player;
	}

	public boolean getIsEditing()
	{
		return edited!=null;
	}

	public void clearActiveHunt()
	{
		activeQuest=null;	
	}

	public void sendMessage(String string)
	{
		player.sendMessage(string);
	}

	public boolean isNewbie()
	{
		// TODO Auto-generated method stub
		return false;
	}

	public int getCompletedHuntCount()
	{
		int count = 0;
		for(AdventurerPlayerQuest hunt :  hunts)
		{
			if(hunt.getIsComplete()) count++;
		}

		return count;
	}

	public int getIncompleteHuntCount()
	{
		int count = 0;
		for(AdventurerPlayerQuest hunt :  hunts)
		{
			if(!hunt.getIsComplete()) count++;
		}

		return count;
	}

	public void startTreasureHunt(Adventure hunt)
	{
		// TODO Auto-generated method stub
		
	}

	public boolean getIsHuntCompleted(Adventure hunt)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public boolean editHunt(Adventure newHunt)
	{
		// If editing already return fail
		if(edited!=null) return false;
		
		// Set editing to this hunt
		edited = newHunt;
		
		// Mark the hunt as being edited
		newHunt.startEditing(this);
		
		// return success
		return true;
	}
	
	public boolean isEditing()
	{
		return(edited!=null);
	}

	public String getOwnerHuntList()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Adventure getEditedHunt()
	{
		return edited;
	}

	public boolean endEditing(DataHandler dataHandler, boolean save)
	{
		if(edited==null) return false;

		boolean ok = true;
		
		if(save)
		{
			// Save prize chest contents
			Prize prize = edited.getPrize();
			if(prize!=null)
			{
				prize.updateContentsFromWorld();
				prize.clearChestContents();
			}	
			ok = dataHandler.saveHunt(edited);
		}
		else
		{
			dataHandler.loadHunt(edited);
			edited.repair();
		}

		if(ok)
		{
			edited.endEditing();
			edited=null;
		}
		
		return ok;
	}
}
