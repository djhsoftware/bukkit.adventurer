package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /threset commands
//
// Author      : Junkman
// =============================================================
public class CommandReset
{
	private final AdventurerPlugin plugin;

	public CommandReset(AdventurerPlugin plugin)
	{
		this.plugin = plugin;
	}

	public boolean execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!player.isPermitted("reset")) return true;

		if (arg.length() == 0)
		{
			plugin.WriteToConsole("Syntax: /th reset [ALL|PLAYERS]");
		}
		else
		{
			// Rebuild database from scratch
			if (arg.equalsIgnoreCase("all"))
			{
		/*		plugin.playerHandler.clearActiveHunts();
				plugin.playerHandler.clearPlayerStats();
				plugin.getHunts().clear();
				plugin.dataHandler.resetDatabase();
				plugin.WriteToConsole(">> Database has been rebuilt");*/
			}
			else if (arg.equalsIgnoreCase("players"))
			{
		/*		plugin.getHunts().syncChestContents();
				plugin.playerHandler.clearActiveHunts();
				plugin.playerHandler.clearPlayerStats();
				plugin.WriteToConsole(">> All player stats cleared");*/
			}
		}

		return true;
	}
}
