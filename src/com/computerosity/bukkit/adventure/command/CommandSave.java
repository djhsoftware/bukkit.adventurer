package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

public class CommandSave
{
	AdventurerPlugin plugin;
	
	public CommandSave(AdventurerPlugin _plugin)
	{
		plugin = _plugin;
	}

	public void execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!player.isPermitted("save")) return;

		// End has a different meaning depending on whether you're editing or not
		if(!player.isEditing())
		{
			player.sendMessage("You are not editing a hunt");
		}
		else
		{
			boolean success = plugin.dataHandler.saveHunt(player.getEditedHunt());
			if(success)
				player.sendMessage("Hunt updates have been saved");
			else
				player.sendMessage("Could not save hunt - validate and fix errors before saving");
		}
	}
}
