package com.computerosity.bukkit.adventure.command;

import com.computerosity.bukkit.adventure.AdventurerPlugin;
import com.computerosity.bukkit.adventure.AdventurerPlayer;

// =============================================================
// Class       : TreasureHuntCommand
//
// Description : Process /th end commands
//
// Author      : Junkman
// =============================================================
public class CommandEnd
{
	AdventurerPlugin plugin;
	public CommandEnd(AdventurerPlugin _plugin)
	{
		plugin = _plugin;
	}

	public boolean execute(AdventurerPlayer player, String arg)
	{
		// Check permissions
		if (!player.isPermitted("end")) return true;

		// End has a different meaning depending on whether you're editing or not
		if(player.isEditing())
		{
			// If the player types /th end cancel then the all changes made since last change
			// will be undone
			boolean save = !arg.equalsIgnoreCase("cancel");
			boolean ok = player.endEditing(plugin.dataHandler,save);
			if(!ok)
			{
				player.sendMessage("Could not save hunt - validate and fix errors before saving");
			}
			else
			{
				if(save)
					player.sendMessage("Hunt updates have been saved");
				else
					player.sendMessage("Reverted to previously saved hunt - some signs may need manual removal");
			}
		}
		else
		{
			// Is the player a registered treasure hunter?
			if (player.getActiveHunt()==null)
				player.sendMessage("You're not on a treasure hunt!");
			else
			{
				player.sendMessage("Ending this treasure hunt, you can resume the hunt by typing /start " + player.getActiveHunt().getHunt().getSecret());
				player.clearActiveHunt();
			}
		}
		return true;
	}
}
