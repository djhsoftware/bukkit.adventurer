package com.computerosity.bukkit.adventure;

import java.util.ArrayList;
import java.util.Hashtable;

import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.computerosity.bukkit.adventure.huntitems.*;

public class AdventurerPlayerQuest
{
	private AdventurerPlayer hunter;
	private Adventure hunt;
	public Hashtable<Clue,Boolean> clues = new Hashtable<Clue,Boolean>();
	public ArrayList<ItemStack> playerInventory = new ArrayList<ItemStack>();
	private boolean isComplete = false;
	
	public AdventurerPlayerQuest(AdventurerPlayer _hunter, Adventure _hunt)
	{
		hunter = _hunter;
		hunt = _hunt;
		for(Clue clue : hunt.clues)
		{
			clues.put(clue,false);
		}
	}

	public Adventure getHunt()
	{
		return hunt;
	}

	public void markClueFound(Clue clue)
	{
		clues.put(clue,true);
	}

	public boolean allCluesFound()
	{
		for(Clue clue : hunt.clues)
		{
			if(!clues.get(clue)) return false;
		}
		
		return true;
	}

	public boolean getIsComplete()
	{
		return isComplete;
	}

	public void setIsComplete(boolean val)
	{
		isComplete = val;
	}

	public void stockWorldChestFromPlayer()
	{
		// Get Prize chest
		Prize prize = hunt.getPrize();
		if(prize==null) return;
		
		// Get Prize chest
		Chest chest = prize.getChest();
		if(chest==null) return;
		
		int slot = 0;
		Inventory inv = chest.getInventory();
		inv.clear();
		for(ItemStack stack : playerInventory)
		{
			//int type = result.getInt("pc_typeid");
			//int amount = result.getInt("pc_amount");
			//ItemStack stack = new ItemStack(type, amount);
			inv.setItem(slot, stack);
			slot++;
		}
		
		// FIX for inventory close
		prize.setLastPlayer(hunter.getUniqueId());
	}

	// When a player first opens a chest we have to load it with items
	// from the hunt 
	public void stockPlayerChestFromHunt()
	{
		playerInventory.clear();
		for(ItemStack stack : hunt.getPrize().getChestContents())
		{
			if(stack!=null) playerInventory.add(new ItemStack(stack.getType(),stack.getAmount()));
		}
	}
	
	public void updatePlayerChestFromWorld()
	{
		Prize prize = hunt.getPrize();
		if(prize==null) return;
		
		Chest chest = prize.getChest();
		if(chest==null) return;
		
		playerInventory.clear();
		Inventory inv = chest.getInventory();
		ItemStack[] contents = inv.getContents();
		for(ItemStack stack : contents)
		{
			if(stack!=null) playerInventory.add(new ItemStack(stack.getType(),stack.getAmount()));
		}
	}

	public int getCluesFound()
	{
		// TODO Auto-generated method stub
		return 0;
	}
}
